import React from 'react';
import {Helmet} from "react-helmet";
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Photo from "./containers/Photo/Photo";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import NewPhoto from "./containers/NewPhoto/NewPhoto";


const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props}/> : <Redirect to={redirectTo}/>;
};


const App = () => {
  const user = useSelector(state => state.users.user);
  return (
    <Layout>
      <Helmet titleTemplate="%s - Gallery"
              defaultTitle="Gallery"/>
      <Switch>
        <ProtectedRoute
          isAllowed={user}
          redirectTo='/login'
          path="/new"
          component={NewPhoto}
        />
        <Route path="/" exact component={Photo}/>
        <Route path="/users/:id" exact component={Photo}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
  );
};

export default App;
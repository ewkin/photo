import photosSlice from "../slices/photoSlice";


export const {
  fetchPhotosRequest,
  fetchPhotoDelete,
  fetchPhotosSuccess,
  fetchPhotosFailure,
  createPhotoSuccess,
  createPhotoRequest,
  createPhotoFailure,
} = photosSlice.actions;
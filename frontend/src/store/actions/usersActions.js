import usersSlice from "../slices/usersSlice";

export const {
  registerRequest,
  registerSuccess,
  registerFailure,
  loginRequest,
  loginSuccess,
  loginFailure,
  logoutSuccess,
  logoutRequest,
  facebookLoginRequest,
  googleLoginRequest
} = usersSlice.actions;

import {createSlice} from "@reduxjs/toolkit";

const name = 'photos';
const photosSlice = createSlice({
  name,
  initialState: {
    photos: [],
    user: null,
    photosLoading: false,
    createPhotoLoading: false,
    createPhotoError: null
  },
  reducers: {
    fetchPhotosRequest: state => {
      state.photosLoading = true;
    },
    fetchPhotoDelete: state => {
      state.photosLoading = true;
    },
    fetchPhotosSuccess: (state, {payload: data}) => {
      state.photosLoading = false;
      state.photos = data.photos;
      state.user = data.user;
    },
    fetchPhotosFailure: state => {
      state.photosLoading = false;
    },
    createPhotoRequest: state => {
      state.createPhotoLoading = true;
    },
    createPhotoSuccess: state => {
      state.createPhotoLoading = false;
    },
    createPhotoFailure: (state, {payload: error}) => {
      state.createPhotoLoading = false;
      state.createPhotoError = error;
    }
  }
});

export default photosSlice;
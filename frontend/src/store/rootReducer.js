import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import photoSlice from "./slices/photoSlice";


const rootReducer = combineReducers({
  users: usersSlice.reducer,
  photos: photoSlice.reducer
});


export default rootReducer;
import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FileInput from "../UI/Form/FileInput";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';


const PhotoForm = ({onSubmit, error, loading}) => {
  const [state, setState] = useState({
    title: '',
    image: '',
  });

  const submitFormHandler = e => {
    e.preventDefault();
    onSubmit(state);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.files[0];
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };


  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }


  return (
    <form onSubmit={submitFormHandler} noValidate>
      <Grid container direction="column" spacing={2}>
        <FormElement
          required
          label="Title"
          name="title"
          value={state.title}
          onChange={inputChangeHandler}
          error={getFieldError('title')}
        />
        <Grid item xs>
          <FileInput
            name='image'
            label='Image'
            onChange={fileChangeHandler}
            error={getFieldError('image')}

          />
        </Grid>
        <Grid item xs>
          <ButtonWithProgress type="submit" color="primary" variant="contained" startIcon={<CloudUploadIcon/>}
                              loading={loading} disabled={loading}>
            Post
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </form>
  );
};

export default PhotoForm;
import React, {useEffect, useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {
  CircularProgress,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
  ListSubheader,
  Modal
} from "@material-ui/core";
import AssignmentIndSharpIcon from '@material-ui/icons/AssignmentIndSharp';
import DeleteForeverSharpIcon from '@material-ui/icons/DeleteForeverSharp';
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotoDelete, fetchPhotosRequest} from "../../store/actions/photosActions";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {historyPush} from "../../store/actions/historyActions";


function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    height: '100%',
  },
  progress: {
    height: theme.spacing(20)
  },
  gridList: {
    width: '100%',
    height: '100%',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  paper: {
    position: 'absolute',
    width: 500,
  },
  pic: {
    width: 500,
  }
}));

const Photo = () => {
    const params = useParams()
    const classes = useStyles();
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.photos.photosLoading);
    const user = useSelector(state => state.users.user);
    const currUser = useSelector(state => state.photos.user);

    const [modalStyle] = useState(getModalStyle);
    const [open, setOpen] = useState(false);
    const [pic, setPic] = useState(null);

    useEffect(() => {
      dispatch(fetchPhotosRequest(params.id));
    }, [dispatch, params.id]);


    const handleOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };
    const showPic = (pic, name) => {
      setPic(<img className={classes.pic} src={pic} alt={name}/>)
      handleOpen()
    };

    const deleteOne = (picId) => {
      dispatch(fetchPhotoDelete({picId, userId: params.id}));
    }


    return (
      <div className={classes.root}>

        {loading ? (
            <Grid container justify='center' alignContent="center" className={classes.progress}>
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>) :
          <GridList className={classes.gridList}>

            <GridListTile key="Subheader" cols={2} style={{height: 'auto'}}>
              <Grid item container direction="row" justify="space-between" alignItems="center">
                <ListSubheader component="div">{currUser ? currUser + "'s photos" : 'All Photos'}</ListSubheader>
                {user ? (
                  <Button onClick={() => dispatch(historyPush('/new'))}>Add photo</Button>
                ) : null}
              </Grid>
            </GridListTile>
            {photos.length === 0 ? <h3>You have not posted any photos</h3> :

              photos.map((tile) => (
                <GridListTile key={tile._id}>
                  <img onClick={() => showPic(tile.image, tile.title)} src={tile.image} alt={tile.title}/>
                  <GridListTileBar
                    title={tile.title}
                    subtitle={<span>by: {tile.user.displayName}</span>}
                    actionIcon={
                      (user?._id ? user._id : 'dd') === params.id ?
                        <IconButton aria-label={`info about ${tile.title}`}
                                    className={classes.icon}
                                    onClick={() => deleteOne(tile._id)}
                        >
                          <DeleteForeverSharpIcon/>
                        </IconButton> :
                        <IconButton aria-label={`info about ${tile.title}`}
                                    className={classes.icon}
                                    onClick={() => dispatch(historyPush('/users/' + tile.user._id))}
                        >
                          <AssignmentIndSharpIcon/>
                        </IconButton>
                    }
                  />
                </GridListTile>
              ))
            }
          </GridList>
        }
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <div style={modalStyle} className={classes.paper}>
            {pic}
          </div>
        </Modal>
      </div>

    );
  }
;

export default Photo;
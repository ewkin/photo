import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import {createPhotoRequest} from "../../store/actions/photosActions";
import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {Typography} from "@material-ui/core";

const NewPhoto = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.photos.createPhotoError);
  const loading = useSelector(state => state.photos.createPhotoLoading);

  const onProductsFormSubmit = productData => {
    dispatch(createPhotoRequest(productData));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          Upload Photo
        </Typography>
      </Grid>
      <Grid item xs>
        <PhotoForm
          onSubmit={onProductsFormSubmit}
          loading={loading}
          error={error}
        />
      </Grid>
    </Grid>
  );
};

export default NewPhoto;
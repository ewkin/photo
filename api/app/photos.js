const express = require('express');
const auth = require("../middleware/auth");
const config = require('../config');
const Photos = require("../models/Photo");
const User = require("../models/User");
const upload = require('../multer').photo;


const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const data = {user: null}
    if (req.query.user) {
      data.photos = await Photos.find({user: {_id: req.query.user}}).populate('user', 'displayName');
      const user = await User.findOne({_id: req.query.user});
      data.user = user.displayName;
      return res.send(data);
    }
    data.photos = await Photos.find().populate('user', 'displayName');
    res.send(data);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.delete('/:id', auth, async (req, res) => {
  try {
    const criteria = {_id: req.params.id, user: {_id: req.user._id}};
    await Photos.deleteOne(criteria);

    res.send({message: "Deleted"});
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const photoData = {
      title: req.body.title,
      image: req.file ? config.url + req.file.filename : null,
      user: req.user._id
    };

    const photo = new Photos(photoData);
    await photo.save();
    res.send({photo});
  } catch (e) {
    res.status(400).send(e);
  }

});

module.exports = router;
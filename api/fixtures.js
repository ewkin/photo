const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);
  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@app',
    password: '1qaz@WSX29',
    token: nanoid(),
    displayName: "John"
  }, {
    email: 'admin@app',
    password: '1qaz@WSX29',
    token: nanoid(),
    displayName: "Mike"
  });

  await Photo.create({
    title: 'Nature',
    user: user,
    image: config.url + 'fixtures/nature1.jpg'
  }, {
    title: 'Nature',
    user: admin,
    image: config.url + 'fixtures/nature2.jpg'
  });


  await mongoose.connection.close();

};

run().catch(console.error);